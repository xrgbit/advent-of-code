(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.8
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.8)

(defun parse-instruction (input)
  (with-input-from-string (s input)
    (list (read s)
          (read s))))

(defun parse-program (input)
  (apply #'vector (utils:map-line #'parse-instruction input)))

(defparameter *test-input* (parse-program "nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6"))

(defparameter *input* (parse-program (utils:read-file "8.dat")))

(defun run (program &optional fix-p)
  "RUNs a PROCESS until it stops. Returns the accumulator and the call history
or NIL if the program doesn't stop. When FIX-P is true, RUN tries to fix a
potential infinite loop by changing one NOP to JMP or one JMP to NOP. Stops when
the first branched halts."
  (catch 'found
    (labels ((aux (fixed-p &optional calls (acc 0) (pc 0))
               (labels ((nop () (values acc (1+ pc)))
                        (acc (n) (values (+ acc n) (1+ pc)))
                        (jmp (n) (values acc (+ pc n))))
                 (prog (instruction op arg)
                  top
                    (when (>= pc (length program))
                      (throw 'found (values acc (reverse calls) :halt)))
                    (when (and (member pc calls))
                      (if fix-p
                          (return)
                          (throw 'found (values acc (reverse calls) :loop))))
                    (setf instruction (aref program pc)
                          op (car instruction)
                          arg (cadr instruction)
                          calls (cons pc calls)
                          (values acc pc) (case op
                                            (nop
                                             (unless fixed-p
                                               (apply #'aux t calls (jmp arg)))
                                             (nop))
                                            (acc
                                             (acc arg))
                                            (jmp
                                             (unless fixed-p
                                               (apply #'aux t calls (nop)))
                                             (jmp arg))))
                    (go top)))))
      (aux (not fix-p)))))

(defun part-1 (&optional (input *test-input*))
  (run input))

(defun part-2 (&optional (input *test-input*))
  (run input t))
