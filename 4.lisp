(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.4
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.4)

(defparameter *test-input* "ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in")

(defparameter *input* (utils:read-file "4.dat"))

(defparameter *invalid-input* "eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:20071")

(defstruct passport
  byr iyr eyr hgt hcl ecl pid cid)

(defun parse-passports (passport-spec)
  (with-input-from-string (s passport-spec)
    (labels ((read-key ()
               (let ((r (make-string 3)))
                 (read-sequence r s :end 3)
                 (intern (string-upcase r) :keyword)))
             (read-colon ()
               (read-char s))
             (read-value ()
               (map 'string #'identity
                    (loop :for c := (read-char s nil)
                          :while c
                          :while (char/= c #\space #\newline)
                          :collect c)))
             (read-key-value ()
               (list (read-key)
                     (progn (read-colon) (read-value))))
             (read-newline ()
               (read-char s))
             (read-key-value-or-newline ()
               (if (char= (peek-char nil s) #\newline)
                   (progn (read-char s) nil)
                   (read-key-value)))
             (eof ()
               (not (peek-char nil s nil)))
             (collect-passports (specs)
               (labels ((rec (rem acc group)
                          (if rem
                              (if (car rem)
                                  (rec (cdr rem)
                                       acc
                                       (append (car rem) group))
                                  (rec (cdr rem)
                                       (cons group acc)
                                       nil))
                              (reverse (cons group acc)))))
                 (rec specs nil nil))))
      (mapcar (lambda (x)
                (apply #'make-passport x))
              (collect-passports (loop :while (not (eof))
                                       :collect (read-key-value-or-newline)))))))

(defun valid-passport-p (passport)
  (with-slots (byr iyr eyr hgt hcl ecl pid cid) passport
    (declare (ignore cid))
    (and byr iyr eyr hgt hcl ecl pid)))

(defstruct unit
  value unit)

(defun parse-unit (unit-spec)
  (multiple-value-bind (value pos) (parse-integer unit-spec :junk-allowed t)
    (make-unit :value value
               :unit (read-from-string unit-spec nil nil :start pos) )))

(defun valid-color-p (color-spec)
  (and (= 7 (length color-spec))
       (char= #\= (char color-spec 0))
       (every #'digit-char-p (subseq color-spec 1))))

(defmacro define-passport (passport-spec)
  (let ((%obj (make-symbol "OBJ"))
        (%slot (make-symbol "SLOT")))
    (labels ((gen-parser-generic ()
               `(defgeneric parse-slot (,%obj ,%slot)))
             (gen-validator-generic ()
               `(defgeneric validate-slot (,%obj ,%slot)))
             (gen-parser-method (name function)
               `(defmethod parse-slot ((,%obj passport) (,%slot (eql ',name)))
                  (setf (slot-value ,%obj ',name)
                        (funcall ,function (slot-value ,%obj ',name)))))
             (gen-validator-method (name function)
               `(defmethod validate-slot ((,%obj passport) (,%slot (eql ',name)))
                  (funcall ,function (slot-value ,%obj ',name))))
             (gen-methods ()
               `(progn
                  ,@(loop :for (name parser validator) :in passport-spec
                          :append (list (gen-parser-method name parser)
                                        (gen-validator-method name validator)))))
             (gen-validator ()
               ;; there can't be any accidental variable capture, since the user
               ;; isn't any outside code being provided to this, unlike the
               ;; PARSER and VALIDATORS
               `(defun valid-passport-p* (passport)
                  (and (valid-passport-p passport)
                       (loop :for slot :in ',(mapcar #'car passport-spec)
                             :always (validate-slot passport slot)))))
             (gen-parse-passport-slots ()
               `(defun parse-passport-slots (passport)
                  (loop :for slot :in ',(mapcar #'car passport-spec)
                        :when (slot-value passport slot)
                          :do (parse-slot passport slot))
                  passport)))
      `(progn
         ,(gen-parser-generic)
         ,(gen-validator-generic)
         ,(gen-methods)
         ,(gen-validator)
         ,(gen-parse-passport-slots)))))

(defun parse-fixed-number (length &rest args)
  (lambda (string)
    (when (and (= (length string) length)
               (every #'digit-char-p string))
      (apply #'parse-integer string args))))

(defun parse-year (string)
  (funcall (parse-fixed-number 4) string))

(defun parse-pid (string)
  (funcall (parse-fixed-number 9) string))

(defun valid-color-p (string)
  (and (= 7 (length string))
       (char= #\# (char string 0))
       (every (lambda (c)
                (digit-char-p c 16))
              (subseq string 1))))

(defun parse-ecl (string)
  (when (and (= (length string) 3)
             (member string '("amb"
                              "blu"
                              "brn"
                              "gry"
                              "grn"
                              "hzl"
                              "oth")
                     :test #'string=))
    (read-from-string string)))

(define-passport
    (;; name parser validity-test
     (byr #'parse-year (lambda (x)
                         (<= 1920 x 2002)))
     (iyr #'parse-year (lambda (x)
                         (<= 2010 x 2020)))
     (eyr #'parse-year (lambda (x)
                         (<= 2020 x 2030)))
     (hgt #'parse-unit (lambda (x)
                         (with-slots (unit value) x
                           (case unit
                             (cm (<= 150 value 193))
                             (in (<= 59 value 76))))))
     (hcl #'identity #'valid-color-p)
     (ecl #'parse-ecl (lambda (x)
                        (find x '(amb blu brn gry grn hzl oth))))
     (pid #'parse-pid #'identity)
     (cid #'identity (constantly t))))

(defun part-1 (&optional (input *test-input*))
  (count-if #'valid-passport-p (parse-passports input)))

(defun part-2 (&optional (input *test-input*))
  (count-if #'valid-passport-p*
            (mapcar #'parse-passport-slots (parse-passports input))))
