(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.2
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.2)

(defparameter *test-input* "1-3 a: abcde
1-3 b: cdefg
2-9 c: ccccccccc")

(defparameter *input* (utils:read-file "2.dat"))

(defun skip-whitespace (string)
  (string-trim '(#\space #\newline) string))

(defun parse-password (password)
  "Parses a PASSWORD into a password structure."
  (let ((password (substitute-if #\space
                                 #'(lambda (x)
                                     (or (char= x #\:)
                                         (char= x #\-)))
                                 password)))
    (with-input-from-string (input password)
      (make-password :min (read input)
                     :max (read input)
                     :letter (read-char input)
                     :password (skip-whitespace (read-line input))))))

(defun valid-password (password)
  "Returns T if the PASSWORD is valid."
  (with-slots (letter min max password) password
    (<= min (count letter password) max)))

(defun valid-password-positional (password)
  "Returns T if the PASSWORD is valid. Using MIN and MAX as the 1-index
position of where _one_ and only _one_ of the positions should contain the
specified letter."
  (flet ((char=-at (c string index)
           (char= c (char string index))))
    (with-slots (letter min max password) password
      (let ((p (char=-at letter password (1- min)))
            (q (char=-at letter password (1- max))))
        (and (or p q) (not (and p q)))))))

(defstruct password
  letter
  min
  max
  password)

(defun count-valid-passwords (input &key (test #'valid-password))
  (count-if test (utils:map-line #'parse-password input)))

(defun part-1 (&optional (input *test-input*))
  (count-valid-passwords input))

(defun part-2 (&optional (input *test-input*))
  (count-valid-passwords input :test #'valid-password-positional))
