(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.1
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.1)

(defparameter *test-input* '(1721
                             979
                             366
                             299
                             675
                             1456))

(defparameter *input* (utils:map-line #'parse-integer (utils:read-file "1.dat")))

(defun pair-sums-to (sum numbers)
  "Finds a pair of numbers in NUMBERS that add up to SUM."
  (loop :for numbers :on numbers
          :thereis (loop :with x := (car numbers)
                         :for y :in (cdr numbers)
                           :thereis (when (= sum (+ x y))
                                      (list x y)))))

(defun triple-sums-to (sum numbers)
  (loop :for numbers :on numbers
          :thereis (loop :with x := (car numbers)
                         :for numbers :on (cdr numbers)
                           :thereis (let ((r (pair-sums-to (- sum x) numbers)))
                                      (when r (cons x r))))))

(defun fix-expense-report (report &key (test #'pair-sums-to))
  (reduce #'* (funcall test 2020 report)))

(defun part-1 (&optional (report *test-input*))
  (fix-expense-report report))

(defun part-2 (&optional (report *test-input*))
  (fix-expense-report report :test #'triple-sums-to))
