(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.3
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.3)

(defparameter *test-input*
  "..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#")

(defparameter *input* (utils:read-file "3.dat"))

(defun parse-tree-map (map)
  "Parses the string MAP into a 2 dimensional vector. The presence of a tree is
represented with T. Indexed by Row then Column."
  (flet ((parse-tree-line (line)
           (map 'list (lambda (x)
                        (char= x #\#))
                line)))
    (utils:list->2d-array (utils:map-line #'parse-tree-line map))))

(defun treep (tree-map row col)
  (aref tree-map row col))

(defun count-tree (map &key (right 0) (down 0))
  (loop :with rows := (car (array-dimensions map))
        :with cols := (cadr (array-dimensions map))
        :for row :from 0 :by down :below rows
        :for col :from 0 :by right
        :count (treep map row (mod col cols))))

(defun part-1 (&key (input *test-input*) (right 3) (down 1))
  (let ((tree-map (parse-tree-map input)))
    (count-tree tree-map :right right :down down)))

(defun part-2 (&optional (input *test-input*))
  (let ((slopes '((1 1)
                  (3 1)
                  (5 1)
                  (7 1)
                  (1 2))))
    (reduce #'* (loop :for (right down) :in slopes
                      :collect (part-1 :input input :right right :down down)))))
