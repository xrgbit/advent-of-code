(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.6
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.6)

(defparameter *test-input* "abc

a
b
c

ab
ac

a
a
a
a

b")

(defparameter *input* (utils:read-file "6.dat"))

(defun count-answers (group &optional (grouping-function #'union))
  (length (reduce grouping-function
                  (mapcar (lambda (x) (coerce x 'list)) group))))

(defun part-1 (&optional (input *test-input*))
  (loop :for group :in (utils:group-into-sublists
                        (utils:map-line #'utils:string-not-empty-p input))
        :sum (count-answers group)))

(defun part-2 (&optional (input *test-input*))
  (loop :for group :in (utils:group-into-sublists
                        (utils:map-line #'utils:string-not-empty-p input))
        :sum (count-answers group #'intersection)))
