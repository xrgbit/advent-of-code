(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.11
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.11)

(defparameter *test-input* "L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL")

(defparameter *input* (utils:read-file "11.dat"))

(defun parse-seat-map (input)
  (flet ((parse-seats (line)
           (loop :for c :across line
                 :collect (if (char= #\L c) :empty :floor))))
    (let ((map (utils:map-line #'parse-seats input)))
      (make-array (list (length map)
                        (length (car map)))
                  :initial-contents map))))

(defun neighbor-indices (x y x-length y-length)
  (remove-if-not (lambda (i &aux (x (car i)) (y (cadr i)))
                   (and (< -1 x x-length)
                        (< -1 y y-length)))
                 (loop :for (dx dy) :in '((-1 -1)  (-1 0)  (-1 1)
                                          (0 -1) #|(0 0)|# (0 1)
                                          (1 -1)   (1 0)   (1 1))
                       :collect (list (+ x dx) (+ y dy)))))

(defun stabalize-seats (seat-map)
  (let ((neighbors (utils:maparray*
                    (lambda (i v) (declare (ignore v))
                      (apply #'neighbor-indices
                             (append i (array-dimensions seat-map))))
                    seat-map)))
    (labels ((neighbors (x y)
               (aref neighbors x y))
             (seated-neighbors (map x y)
               (loop :for (x y) :in (neighbors x y)
                     :count (eq :occupied (aref map x y))))
             (new-status (status neighbor-count)
               (if (eq :floor status)
                   :floor
                   (cond ((zerop neighbor-count) :occupied)
                         ((>= neighbor-count 4) :empty)
                         (t status))))
             (update-seats (map)
               (utils:maparray*
                (lambda (i v)
                  (new-status v (apply #'seated-neighbors map i)))
                map)))
      (loop :for map := seat-map :then next-map
            :for next-map := (update-seats map)
            :until (equalp map next-map)
            :finally (return next-map)))))

(defun seats-occupied (seat-map)
  (loop :for i :from 0 :below (array-total-size seat-map)
        :count (eq :occupied (row-major-aref seat-map i))))

(defun part-1 (&optional (input *test-input*))
  (seats-occupied (stabalize-seats (parse-seat-map input))))

(defun part-2 (&optional (input *test-input*))
  )
