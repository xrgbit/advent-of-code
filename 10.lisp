(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.10
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.10)

(defparameter *example* "16
10
15
5
1
11
7
19
6
12
4")

(defparameter *test-input* "28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3")

(defparameter *input* (utils:read-file "10.dat"))

(defun parse-adapters (input)
  (let ((adapters (sort (utils:map-line #'parse-integer input) #'<)))
    (cons 0 (append adapters (list (+ 3 (car (last adapters))))))))

(defun differences (list)
  "Returns a list of the difference between each pair of items of the list."
  (loop :for (x y) :on list
        :while (and x y)
        :collect (- y x)))

(defun part-1 (&optional (input *test-input*))
  (let ((r (differences (parse-adapters input))))
    (* (count 1 r) (count 3 r))))

(defun branchings (list)
  (let ((table (make-hash-table)))
    (flet ((branchings (list)
             (loop :for x :in (cdr list)
                   :for delta := (- x (car list))
                   :while (<= delta 3)
                   :collect x)))
      (loop :for x :on list
            :do (setf (gethash (car x) table) (branchings x)))
      table)))

(defun branches (branchings)
  (utils::with-cache ()
    (labels ((value (k)
               (getcache k
                         (let ((v (gethash k branchings)))
                           (if (null v)
                               1
                               (reduce #'+ (mapcar #'value v)))))))
      (value 0))))

(defun part-2 (&optional (input *test-input*))
  (branches (branchings (parse-adapters input))))
