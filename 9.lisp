(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.9
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.9)

(defparameter *test-input* "35
20
15
25
47
40
62
55
65
95
102
117
150
182
127
219
299
277
309
576")

(defparameter *input* (utils:read-file "9.dat"))

(defun parse-numbers (input)
  (utils:map-line #'read-from-string input))

(defun pair-sums-to (sum numbers &key end)
  "Finds a pair of numbers in NUMBERS that add up to SUM. Uses only END numbers
from NUMBERS."
  (loop :for numbers :on (subseq numbers 0 end)
          :thereis (loop :with x := (car numbers)
                         :for y :in (cdr numbers)
                           :thereis (when (= sum (+ x y))
                                      (list x y)))))

(defun contiguous-sums-to (sum numbers)
  "Finds a sublist of NUMBERS with SUM."
  (loop :for rem :on numbers
          :thereis
          (loop :for n :in rem
                :for acc := 0 :then (+ acc n)
                :for ns := () :then (cons n ns)
                :while rem
                  :thereis (and (= sum acc) ns))))

(defun invalid-p (numbers preamble-length)
  (unless (pair-sums-to (car numbers) (cdr numbers) :end preamble-length)
    (car numbers)))

(defun first-invalid (numbers preamble-length)
  (loop :for rem :on (reverse numbers)
        :while (< preamble-length (1+ (length rem)))
          :thereis (invalid-p rem preamble-length)))

(defun part-1 (&optional (input *test-input*) (preamble-length 5))
  (first-invalid (parse-numbers input) preamble-length))

(defun sum-ends (list)
  "Adds the first and last items of LIST."
  (+ (car list) (car (last list))))

(defun part-2 (&optional (input *test-input*) (preamble-length 5))
  (let* ((numbers (parse-numbers input))
         (invalid (first-invalid numbers preamble-length)))
    (sum-ends (sort (contiguous-sums-to invalid numbers) #'<))))
