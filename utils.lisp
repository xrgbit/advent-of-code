(defpackage #:rgbit.aoc.utils
  (:use #:cl)
  (:export
   #:read-file
   #:string-not-empty-p
   #:map-line
   #:list->2d-array
   #:concat-symbol
   #:enlist
   #:group-into-sublists
   #:caching
   #:with-cache
   #:maparray
   #:maparray*))

(in-package #:rgbit.aoc.utils)

(defun read-file (filespec)
  "Reads in the file specified by FILESPEC as a string."
  (with-open-file (f filespec)
    (let ((string (make-string (file-length f))))
      (read-sequence string f)
      string)))

(defun string-not-empty-p (string)
  (unless (string= string "") string))

(defun map-line (fn string)
  "Maps FN on each line (delimited as by READ-LINE) of STRING"
  (with-input-from-string (s string)
    (loop :for line := (read-line s nil)
          :while line
          :collect (funcall fn line))))

(defun group-into-sublists (list)
  "Takes a list of items separated by NIL and groups them into sublists.
(GROUP-INTO-SUBLISTS '(\"abc\" NIl \"a\" \"b\" \"c\" nil \"ab\" \"ac\" nil \"c\") nil)
  => ((\"abc\") (\"a\" \"b\" \"c\") (\"ab\" \"ac\") (\"c\") ())
"
  (labels ((rec (rem acc group)
             (if rem
                 (if (car rem)
                     (rec (cdr rem)
                          acc
                          (cons (car rem) group))
                     (rec (cdr rem)
                          (cons (reverse group) acc)
                          nil))
                 (reverse (cons (reverse group) acc)))))
    (rec list nil nil)))

(defun list->2d-array (list)
  (make-array (list (length list)
                    (length (car list)))
              :initial-contents list))

(defun concat-symbol (&rest rest)
  (intern (format nil "~{~a~}" rest)))

(defun enlist (obj)
  (if (listp obj)
      obj
      (list obj)))

(defmacro caching ((key cache) &body body)
  "Tries to find the value KEY in the CACHE. If found, KEY is returned.
Otherwise the result of BODY is returned and saved into CACHE under KEY."
  (let ((v (make-symbol "VALUE"))
        (found-p (make-symbol "FOUND-P")))
    `(multiple-value-bind (,v ,found-p) (gethash ,key ,cache)
       (if ,found-p
           ,v
           (setf (gethash ,key ,cache)
                 ,@body)))))

(defmacro with-cache ((&rest args) &body body)
  "Wraps BODY with a hash table (made using ARGS) called CACHE.
 Creates a function GETCACHE that can be called with 2 arguments (a key and a
 value). It will cache the value under the key if no value matching the key is
 found in cache, otherwise it just returns the value found."
  (let ((k (make-symbol "KEY"))
        (v (make-symbol "VALUE"))
        (cache (make-symbol "CACHE")))
    `(let ((,cache (make-hash-table ,@args)))
       (macrolet ((,(intern (symbol-name '#:getcache)) (,k ,v)
                    `(caching (,,k ,',cache) ,,v)))
         ,@body))))

(defmacro with-cache* ((&rest args) &body body)
  "Wraps BODY with a hash table (made using ARGS) called CACHE.
 Creates a function GETCACHE that can be called with 2 arguments (a key and a
 value). It will cache the value under the key if no value matching the key is
 found in cache, otherwise it just returns the value found."
  (let ((k (make-symbol "KEY"))
        (v (make-symbol "VALUE"))
        (cache (make-symbol "CACHE")))
    `(let ((,cache (make-hash-table ,@args)))
       (flet ((,(intern (symbol-name '#:getcache)) (,k ,v)
                (caching (,k ,cache) ,v)))
         ,@body))))

(defun array-row-major-substructs (array index)
  (loop :for i := index :then remainder
        :for size :in (append
                       (maplist (lambda (x)
                                  (reduce #'* x))
                                (cdr (array-dimensions array)))
                       (list 1))
        :for (divisor remainder) := (multiple-value-list (truncate i size))
        :collect divisor))

(defun %maparray (function array &optional with-dimensions-p)
  (let ((r (make-array (array-dimensions array))))
    (loop :for i :from 0 :below (array-total-size array)
          :do (setf (row-major-aref r i)
                    (if with-dimensions-p
                        (funcall function (array-row-major-substructs array i)
                                 (row-major-aref array i))
                        (funcall function (row-major-aref array i))))
          :finally (return r))))

(defun maparray (function array)
  "Maps FUNCTION on every value of ARRAY creating a new array with the same
dimensions as ARRAY."
  (%maparray function array))

(defun maparray* (function array)
  "Maps FUNCTION on every index and value of ARRAY creating a new array with the
same dimensions as ARRAY."
 (%maparray function array t))
