(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.12
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.12)

(defparameter *test-input* "F10
N3
F7
R90
F11")

(defparameter *input* (utils:read-file "12.dat"))

(defun degree->radian (degrees)
  (/ (* degrees pi) 180))

(defun rotate (direction degree)
  (let ((x (realpart direction))
        (y (imagpart direction))
        (radian (degree->radian degree)))
    (complex (round (- (* x (cos radian))
                       (* y (sin radian))))
             (round (+ (* y (cos radian))
                       (* x (sin radian)))))))

(defun direction->vector (direction)
  (ecase direction
    (n #C(0 1))
    (s #C(0 -1))
    (e #C(1 0))
    (w #C(-1 0))))


(defun move-ship (instructions &optional waypoint-p)
  (loop :with pos := #C(0 0)
        :with waypoint := (if waypoint-p #C(10 1) #C(1 0))
        :for (op arg) :in instructions
        :do (case op
              ((n s e w) (let ((dpos (* arg (direction->vector op))))
                           (if waypoint-p
                               (incf waypoint dpos)
                               (incf pos dpos))))
              (f (incf pos (* arg waypoint)))
              (l (setf waypoint (rotate waypoint arg)))
              (r (setf waypoint (rotate waypoint (- arg)))))
        :finally (return pos)))

(defun parse-directions (input)
  (flet ((parse-instruction (line)
           (list (read-from-string line nil nil :end 1)
                 (parse-integer line :start 1))))
    (utils:map-line #'parse-instruction input)))

(defun manhattan-distance (point)
  (+ (abs (realpart point)) (abs (imagpart point))))

(defun part-1 (&optional (input *test-input*))
  (manhattan-distance (move-ship (parse-directions input))))

(defun part-2 (&optional (input *test-input*))
  (manhattan-distance (move-ship (parse-directions input) t)))
