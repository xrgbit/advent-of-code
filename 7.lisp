(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.7
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.7)

(defparameter *test-input* "light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.")

(defparameter *input* (utils:read-file "7.dat"))

(defun read-chars (n stream)
  (loop :repeat n
        :do (read-char stream nil)))

(defun parse-count (stream)
  "`no' | NUMBER"
  (let ((r (read stream nil)))
    (if (eq 'no r) nil r)))

(defun parse-bag (stream)
  "DESCRIPTION COLOR `bags' | \"other\" `bags'
(PARSE-BAG \"light red bags\")
=> LIGHT-RED
(PARSE-BAG \"other bags\")
=> NIL
"
  (let ((r (read stream nil)))
    (prog1 (if (eq 'other r) nil
               (utils:concat-symbol r '#:- (read stream nil)))
      ;; "bags." will also catch "bags,"
      (read-chars (length "bags.") stream))))

(defun parse-counted-bag (stream)
  (let ((count (parse-count stream)))
    (when count
      (list count (parse-bag stream)))))

(defun parse-contained-bags (stream)
  "`no other bags' | NUMBER BAG
(PARSE-CONTAINED-BAGS \"contain 1 bright white bag, 2 muted yellow bags.\")
=> ((1 BRIGHT-WHITE) (2 MUTED-YELLOW))
"
  ;; READs `contain'
  (read-chars (length "contains") stream)
  (loop :for bag := (parse-counted-bag stream)
        :while bag
        :collect bag))

(defun parse-rule (rule)
  "BAG `bag' `contains' [contained-bags ,...] 
(PARSE-RULE \"light red bags contain 1 bright white bag, 2 muted yellow bags.\")
=> (LIGHT-RED (1 BRIGHT-WHITE) (2 MUTED-YELLOW))"
  (with-input-from-string (s rule)
    (cons (parse-bag s) (parse-contained-bags s))))

(defun parse-rules (rules)
  (utils:map-line #'parse-rule rules))

(defun make-containing-table (rules)
  (let ((table (make-hash-table :test #'eq)))
    (loop :for (bag . contained-bags) :in rules
          :do (loop :for (nil contained-bag) :in contained-bags
                    :do (pushnew bag (gethash contained-bag table))))
    table))

(defun containing-bags (rules bag)
  (let ((table (make-containing-table rules)))
    (labels ((get-bags (bag)
               (gethash bag table))
             (rec (bags)
               (loop :for bag :in bags
                     :append (cons bag (rec (get-bags bag))))))
      (remove-duplicates (rec (get-bags bag))))))

(defun contained-bags (rules bag)
  (labels ((get-bags (bag)
             (cdr (find bag rules :key #'car :test #'eq)))
           (count-bags (bag)
             (1+ (loop :for (count bag) :in (get-bags bag)
                       :sum (* count (count-bags bag))))))
    (1- (count-bags bag))))


(defun part-1 (&optional (input *test-input*))
  (length (containing-bags (parse-rules input) 'shiny-gold)))

(defun part-2 (&optional (input *test-input*))
  (contained-bags (parse-rules input) 'shiny-gold))
