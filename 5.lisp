(load "utils.lisp")

(defpackage #:rgbit.aoc.2020.5
  (:use #:cl)
  (:local-nicknames (#:utils #:rgbit.aoc.utils)))

(in-package #:rgbit.aoc.2020.5)

(defparameter *test-input* "")

(defparameter *input* (utils:read-file "5.dat"))

(defun parse-binary-number (string zeros ones
                            &aux (zeros (utils:enlist zeros))
                              (ones (utils:enlist ones)))
  (parse-integer
   (map 'string (lambda (c)
                  (cond ((member c zeros) #\0)
                        ((member c ones) #\1)))
        string)
   :radix 2))

(defun seat-id (string)
  (parse-binary-number string '(#\F #\L) '(#\B #\R)))

(defun part-1 (&optional (input *test-input*))
  (loop :for x :in (utils:map-line #'seat-id input)
        :maximize x))

(defun part-2 (&optional (input *test-input*))
  (loop :for (row next) :on (sort (utils:map-line #'seat-id input) #'<)
          :thereis (and (/= (1+ row) next)
                        (1+ row))))
